---
title: Privacy Policy
---
## 1. Web analytics

This web site does not use web analytics.

## 2. Cookies

This web site does not store tracking cookies on your device.

## 3. Hosting provider

This web site is hosted using [Codeberg Pages](https://codeberg.page/), check out [their privacy policy](https://codeberg.org/Codeberg/org/src/branch/main/PrivacyPolicy.md).
