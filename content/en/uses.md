---
title: Programs and Equipment I Use
date: 2024-10-02
---
## Hardware I Use

* HP Laptop 14s dq2024nf [Laptop]
* MacBook Pro (13-inch, Late 2011) [Home Server]
* Samsung Galaxy A15 [Phone]

## Software I Use

### Computer

* Operating System / Distribution: NixOS
* Terminal: Konsole
* Shell: Bash
* Window Manger: niri
* Text editor: Helix
* Web browser: Mullvad Browser
* File manager: Nautilus
* Email client: Evolution
* Music player: Strawberry Music Player
* Video player: VLC
* RSS reader: Newsboat
* Torrent client: Fragments

### Phone

* Operating System / Distribution: One UI
* System apps: Samsung's default
* App stores: Obtainium, Aurora Store
* Web browser: Brave
* Instant messaging: Signal
* YouTube client: Grayjay

## Productivity

* I use a self hosted CardDAV/CalDAV instanse for my Calendar Events, Tasks and Notes
* And I usr a self hosted Firefly III instanse to track my expenses
