---
title: Contact me
---
## Signal

- [@rayan.75](https://signal.me/#eu/Pe8Q9pEEiRPgiIAjTyL51DehB8jin1JW6a2-9HOGU6BTrjU4wZZO3K3bgqAbFfCG)

## Email

- [nakibrayan@disroot.org](mailto:nakibrayan@disroot.org)

- Download [public PGP key](/assets/63b28f38777db6542089005c154c4f3a0b4b28b9.asc) to encrypt email (expires on: Sep 14, 2025).

- Key fingerprint: 63B2 8F38 777D B654 2089  005C 154C 4F3A 0B4B 28B9
