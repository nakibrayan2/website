---
title: What I’m doing now
date: 2024-10-25
---
## What I’m reading

* 📕 [The Rust Programming Language, 2nd Edition](https://nostarch.com/rust-programming-language-2nd-edition)

## What I’m Listening to

* 🎙️ [Huberman Lab](https://hubermanlab.com)
* 🎙️ [Surveillance Report](https://surveillancereport.tech)
* 🎵 [Fireworks & Rollerblades](https://store.bensonboone.com/products/fireworks-rollerblades-cd)
* 🎵 [Flou](https://open.spotify.com/intl-ar/album/67cpbnbv3A8dewZVqfHams)

## What I’m watching

* 🎬 [The Lord of the Rings](https://en.wikipedia.org/wiki/The_Lord_of_the_Rings_(film_series))

## What I'm learning

* 👨‍💻 [Rust](https://www.rust-lang.org)

## What I'm working on

* 🎓 busy studying